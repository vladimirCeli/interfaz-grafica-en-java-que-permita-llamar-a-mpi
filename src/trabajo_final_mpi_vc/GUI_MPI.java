/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package trabajo_final_mpi_vc;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author VLadimir Celi .
 */
public class GUI_MPI extends javax.swing.JFrame {

    Methods method = new Methods();
    String resultadoUbicacion = "";

    public GUI_MPI() {
        initComponents();
        verNucleos();
        this.setLocationRelativeTo(null);
    }

    public void verNucleos(){
        String comandoVerNucleos = "lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l";
        Process verNucleos = method.comandos(comandoVerNucleos);
        Integer nucleosDisponibles = Integer.parseInt(method.revision(verNucleos).trim());
        int i = 0;
        String datos = "";
        while (i < nucleosDisponibles){
            datos = String.valueOf(i+1);
            JCNucleos.addItem(datos);
            i++;
        }
    }
    
    private String abrirUbicacionArchivo() {
        try {
            JFileChooser file = new JFileChooser();
            file.showOpenDialog(this);
            File abrir = file.getSelectedFile();
            if (abrir != null) {
                resultadoUbicacion = abrir.getAbsolutePath();
                jLubicacion.setText(resultadoUbicacion);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + ""
                    + "\nNo se ha encontrado el archivo",
                    "ADVERTENCIA!!!", JOptionPane.WARNING_MESSAGE);
        }
        return resultadoUbicacion;
    }
    
    private void ejecutar () {
        String EleccionNucleos = (String) JCNucleos.getSelectedItem();
        System.out.println(EleccionNucleos);
        String comandoEjecutar = "mpirun -np ";
        String cadenaResultante = comandoEjecutar+EleccionNucleos;
        int cantEspacios = 0;
        String comandoResultante = "";
        for (int i = 0; i < resultadoUbicacion.length(); i++) {
            if (resultadoUbicacion.charAt(i)==' '){
                comandoResultante = resultadoUbicacion.replace(" ","\\ ");        
            }
        }
        if (comandoResultante=="") comandoResultante=resultadoUbicacion;
        Process ejec = method.comandos(cadenaResultante+" "+comandoResultante);
        System.out.println(comandoResultante);
        JTerminal.setText(method.revision(ejec));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTerminal = new javax.swing.JTextArea();
        JbtnSA = new javax.swing.JButton();
        JbtnEjec = new javax.swing.JButton();
        jLubicacion = new javax.swing.JLabel();
        JCNucleos = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Liberation Sans", 1, 36)); // NOI18N
        jLabel1.setText("Ejecución de MPI de forma Local");

        jLabel2.setText("Nombre: Vladimir Celi");

        JTerminal.setColumns(20);
        JTerminal.setRows(5);
        jScrollPane1.setViewportView(JTerminal);

        JbtnSA.setText("Seleccionar Archivo");
        JbtnSA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JbtnSAMouseClicked(evt);
            }
        });

        JbtnEjec.setText("Ejecutar");
        JbtnEjec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JbtnEjecActionPerformed(evt);
            }
        });

        jLabel3.setText("Ver núcleos de los procesadores");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addComponent(jLabel2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(232, 232, 232)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 582, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLubicacion, javax.swing.GroupLayout.PREFERRED_SIZE, 704, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 704, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(JbtnSA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(JCNucleos, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(158, 158, 158)
                                .addComponent(JbtnEjec)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(258, 258, 258))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(jLabel2)
                .addGap(7, 7, 7)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JbtnSA)
                    .addComponent(JbtnEjec)
                    .addComponent(JCNucleos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addComponent(jLubicacion, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JbtnSAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JbtnSAMouseClicked
        abrirUbicacionArchivo();
    }//GEN-LAST:event_JbtnSAMouseClicked

    private void JbtnEjecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JbtnEjecActionPerformed
        // TODO add your handling code here:
        ejecutar();
    }//GEN-LAST:event_JbtnEjecActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI_MPI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> JCNucleos;
    private javax.swing.JTextArea JTerminal;
    private javax.swing.JButton JbtnEjec;
    private javax.swing.JButton JbtnSA;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLubicacion;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
