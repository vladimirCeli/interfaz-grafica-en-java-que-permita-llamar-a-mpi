/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package trabajo_final_mpi_vc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vladimir Celi
 */
public class Methods {
    
    public String revision(Process proceso){
        String resulLineas = "";
        String leerLinea = "";
        try {
            InputStreamReader ingreUbi = new InputStreamReader(proceso.getInputStream());
            BufferedReader archiv = new BufferedReader(ingreUbi);
            leerLinea = archiv.readLine();
            do {
                resulLineas += leerLinea+"\n";
                leerLinea = archiv.readLine();
            } while (leerLinea != null);
        } catch (IOException ex) {
            Logger.getLogger(Methods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resulLineas;
    }
    
    
    public Process comandos(String comando){
        Process resulComando = null;
        try {
            String arrayComandos [] = {"bash", "-c", comando};
            resulComando = Runtime.getRuntime().exec(arrayComandos);
        } catch (IOException ex) {
           Logger.getLogger(Methods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resulComando;
    }
    
}
